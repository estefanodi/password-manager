from PyQt5.QtWidgets import QMessageBox, QPushButton
from PyQt5.QtGui import QPixmap

def show_dialog(text, typ, window):
    msgBox = QMessageBox(window)
    msgBox.setIcon(QMessageBox.Information)
    msgBox.setIconPixmap(
        QPixmap('./assets/information.png' if typ == 'information' else './assets/warning.png'))
    msgBox.setText(f'<strong>{text}</strong>')
    msgBox.setStyleSheet('background-color:gray; color: white;')
    acceptButton = QPushButton('Yes')
    # acceptButton.setStyleSheet(
    #      'background-color: green')    
    rejectButton = QPushButton('No')
    # rejectButton.setStyleSheet(
    #        'background-color: red')
    okButton = QPushButton('Ok')
    okButton.setStyleSheet(
           'background-color: rgb(105,105,105); color: white;')
    if typ == 'warning':
        msgBox.addButton(acceptButton, QMessageBox.AcceptRole)
        msgBox.addButton(rejectButton, QMessageBox.RejectRole)
    if typ == 'information':
        msgBox.addButton(okButton, QMessageBox.AcceptRole)
    msgBox.setWindowTitle("Information" if typ == 'information' else 'warning')
    returnValue = msgBox.exec()
    return returnValue
