checkBox_styles = '''
                QCheckBox::indicator { width : 12; height : 12; border: 1px solid gray; border-radius: 3px;}
                QCheckBox::indicator:checked {
                            background-color:qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:0, stop:0 rgba(119, 20, 213, 255), stop:0.085 rgba(154, 0, 197, 255), stop:0.19 rgba(156, 30, 202, 255), stop:0.275 rgba(189, 49, 236, 255), stop:0.39 rgba(194, 34, 243, 255), stop:0.555 rgba(175, 32, 247, 255), stop:0.667 rgba(124, 75, 255, 255), stop:0.802743 rgba(167, 23, 255, 255), stop:0.885 rgba(168, 22, 222, 255), stop:1 rgba(210, 0, 215, 255));
                            color: white; 
                            image : url(./assets/checked.png);
                }
                QCheckBox::indicator:unchecked {
                            background-color : transparent;
                }
                '''
slider_styles = '''
                QSlider::groove:horizontal {
                       
                        background: rgba(0,0,0, 0.2);
                        height: 10px;
                        border-radius: 4px;
                }
                QSlider::sub-page:horizontal {
                        background: qlineargradient(x1: 0, y1: 0,    x2: 0, y2: 1,
                        stop: 0 #66e, stop: 1 #bbf);
                        background: qlineargradient(x1: 0, y1: 0.2, x2: 1, y2: 1,
                        stop: 0 #bbf, stop: 1 #55f);
                        border: 1px solid #777;
                        height: 10px;
                        border-radius: 4px;
                }
                QSlider::add-page:horizontal {
                        background: transparent;
                        height: 10px;
                        border-radius: 4px;
                }
                QSlider::handle:horizontal {
                        background-color: rgb(194, 108, 219);
                        width: 13px;
                        margin-top: -2px;
                        margin-bottom: -2px;
                        border-radius: 4px;
                }
                QSlider::sub-page:horizontal:disabled {
                        background: #bbb;
                        border-color: #999;
                }
                QSlider::add-page:horizontal:disabled {
                        background: #eee;
                        border-color: #999;
                }
                QSlider::handle:horizontal:disabled {
                        background: #eee;
                        border: 1px solid #aaa;
                        border-radius: 4px;
                }
                '''

header_styles = '''
                  background-color:qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:0, stop:0 rgba(119, 20, 213, 255), stop:0.085 rgba(154, 0, 197, 255), stop:0.19 rgba(156, 30, 202, 255), stop:0.275 rgba(189, 49, 236, 255), stop:0.39 rgba(194, 34, 243, 255), stop:0.555 rgba(175, 32, 247, 255), stop:0.667 rgba(124, 75, 255, 255), stop:0.802743 rgba(167, 23, 255, 255), stop:0.885 rgba(168, 22, 222, 255), stop:1 rgba(210, 0, 215, 255));
                  
                  border-radius: 6px;
                '''

button_styles = '''
                background-color:qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:0, stop:0 rgba(119, 20, 213, 255), stop:0.085 rgba(154, 0, 197, 255), stop:0.19 rgba(156, 30, 202, 255), stop:0.275 rgba(189, 49, 236, 255), stop:0.39 rgba(194, 34, 243, 255), stop:0.555 rgba(175, 32, 247, 255), stop:0.667 rgba(124, 75, 255, 255), stop:0.802743 rgba(167, 23, 255, 255), stop:0.885 rgba(168, 22, 222, 255), stop:1 rgba(210, 0, 215, 255));
                
                border-radius: 6px;
               '''

rounded_button_styles = '''
                background-color:qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:0, stop:0 rgba(119, 20, 213, 255), stop:0.085 rgba(154, 0, 197, 255), stop:0.19 rgba(156, 30, 202, 255), stop:0.275 rgba(189, 49, 236, 255), stop:0.39 rgba(194, 34, 243, 255), stop:0.555 rgba(175, 32, 247, 255), stop:0.667 rgba(124, 75, 255, 255), stop:0.802743 rgba(167, 23, 255, 255), stop:0.885 rgba(168, 22, 222, 255), stop:1 rgba(210, 0, 215, 255));
                border-radius: 25px;
                '''

small_button_styles = '''
                background-color:qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:0, stop:0 rgba(119, 20, 213, 255), stop:0.085 rgba(154, 0, 197, 255), stop:0.19 rgba(156, 30, 202, 255), stop:0.275 rgba(189, 49, 236, 255), stop:0.39 rgba(194, 34, 243, 255), stop:0.555 rgba(175, 32, 247, 255), stop:0.667 rgba(124, 75, 255, 255), stop:0.802743 rgba(167, 23, 255, 255), stop:0.885 rgba(168, 22, 222, 255), stop:1 rgba(210, 0, 215, 255));
                border-radius: 5px;
                '''

password_label_styles = '''
                        
                        background-color: rgba(0,0,0,0.2);
                        border-radius: 7px;
                        '''

small_label_styles = '''
                border-radius: 6px;
                background-color:rgba(255,255,255,0.2);
                color: white;
                '''