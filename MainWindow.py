import string, random, sys, pyperclip
from PyQt5.QtWidgets import QApplication, QMainWindow, QMessageBox
from PyQt5.QtGui import QIcon
from PyQt5.QtCore import QTimer
from PyQt5 import QtCore
from PyQt5.QtCore import * 

from Ui_MainWindow import Ui_MainWindow
from Ui_splash import Ui_splash

from dialog import show_dialog
counter = 0
# ! ========================================================================
# ! ==========================  APPLICATION  ===============================
# ! ========================================================================
class MainWindow:
    def __init__(self):
        self.main_win = QMainWindow()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self.main_win)

        # self.setWindowTitle('title')

        self.settings = {
            "uppercase": False,
            "lowercase": False,
            "numbers": False,
            "symbols": False
        }

        self.sliderValue = 10
        self.progress_bar_value = 1
        # * ========> NAVIGATION
        self.ui.stackedWidget.setCurrentWidget(self.ui.generator)
        self.ui.list_pushButton.clicked.connect(self.go_to_list)
        self.ui.back_pushButton.clicked.connect(self.go_to_generator)
        # * ========> SLIDER
        self.ui.length_number_label.setText(str(self.sliderValue))
        self.ui.slider.valueChanged.connect(self.slider_change)
        # * ========> CHECK BOX
        self.ui.uppercase_checkBox.toggled.connect(lambda: self.click_box("uppercase"))
        self.ui.lowercase_checkBox.toggled.connect(lambda: self.click_box("lowercase"))
        self.ui.symbol_checkBox.toggled.connect(lambda: self.click_box("symbols"))
        self.ui.number_checkBox.toggled.connect(lambda: self.click_box("numbers"))
        self.ui.uppercase_checkBox.setChecked(True)
        self.ui.lowercase_checkBox.setChecked(True)
        self.ui.symbol_checkBox.setChecked(True)
        self.ui.number_checkBox.setChecked(True)
        # * ========> GENERATE PASSWORD
        self.ui.generate_pushButton.clicked.connect(self.generate_password)
        # * ========> COPY PASSWORD
        self.ui.copy_pushButton.clicked.connect(self.copy_password)
# * ====================================================================
# * =========================  NAVIGATION  =============================
# * ====================================================================
    def show(self):
        self.main_win.show()
    def go_to_generator(self):
        self.ui.stackedWidget.setCurrentWidget(self.ui.generator)
    def go_to_list(self):
        self.ui.stackedWidget.setCurrentWidget(self.ui.list)
# * ====================================================================
# * =========================  CHECK BOX  ==============================
# * ====================================================================   
    def click_box(self, key):
        self.settings[key] = not self.settings[key]
# * ====================================================================
# * ===========================  SLIDER  ===============================
# * ====================================================================
    def slider_change(self,value):
        self.sliderValue = value
        self.ui.length_number_label.setText(str(value))
# * ====================================================================
# * =====================  GENERATE PASSWORD  ==========================
# * ====================================================================
    def generate_password(self, num):
        is_one_selected = False
        for key in self.settings:
            if self.settings[key]:
                is_one_selected = True
                break
        if not is_one_selected:
            return show_dialog("Please select one of the params",'information',self.main_win)
        data = {
           "lowercase": string.ascii_lowercase,
           "uppercase": string.ascii_uppercase,
           "numbers": string.digits,
           "symbols": "_-*&#@!"
        }
        temp_string = ''
        for key in self.settings:
            if self.settings[key]:
                temp_string += data[key]
        printable = list(temp_string)
        random.shuffle(printable)
        final = random.choices(printable,k=self.sliderValue)
        self.password = ''.join(final)
        self.ui.password_label.setText(''.join(final))
# * ====================================================================
# * =======================  COPY PASSWORD  ============================
# * ====================================================================
    def copy_password(self):
        try:
            pyperclip.copy(self.password)
            self.ui.label_copied.setText("copied")
            QtCore.QTimer.singleShot(1000, lambda: self.ui.label_copied.setText(""))
        except:
            print('error')
# * ====================================================================
# * ========================  SET NEW NAME  ============================
# * ====================================================================
    def set_new_name(self,text):
        self.name = text
# * ====================================================================
# * ========================  SET NEW PASSWORD  ========================
# * ====================================================================
    def set_new_password(self,text):
        self.password = text
    # self.setWindowTitle('title')
# ! ========================================================================
# ! ==========================  SPLASH SCREEN  =============================
# ! ========================================================================
class SplashScreen(QMainWindow):
    def __init__(self):
        QMainWindow.__init__(self)
        self.ui = Ui_splash()
        self.ui.setupUi(self)
        ## REMOVE TITLE BAR
        self.setWindowFlag(QtCore.Qt.FramelessWindowHint)
        self.setStyleSheet("background-color: rgb(46, 52, 54);")
        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.progress)
        self.timer.start(35)
        self.ui.label_description.setText("<strong>Welcome to the password manager</strong>")

        # Change Texts
        QtCore.QTimer.singleShot(1500, lambda: self.ui.label_description.setText("<strong>Loading Passwords .</strong>"))
        QtCore.QTimer.singleShot(2200, lambda: self.ui.label_description.setText("<strong>Loading Passwords ..</strong>"))
        QtCore.QTimer.singleShot(2900, lambda: self.ui.label_description.setText("<strong>Loading Passwords ...</strong>"))
        self.show()

    def progress(self):

        global counter
        self.ui.progressBar.setValue(counter)
        if counter > 100:
            self.timer.stop()
            self.close()
            self.main = MainWindow()
            self.main.show()
        counter += 1

if __name__ == "__main__":
    app = QApplication(sys.argv)
    app.setWindowIcon(QIcon('assets/padlock.png'))
    window = SplashScreen()
    sys.exit(app.exec_())

# if __name__ == '__main__':
#     app = QApplication(sys.argv)
#     app.setWindowIcon(QIcon('assets/padlock.png')) 
#     main_win = MainWindow()
#     main_win.show()
#     sys.exit(app.exec_())