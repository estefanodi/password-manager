from PyQt5.QtGui import QFont

def custom_font(fontType,fontSize):
    font = QFont()
    font.setFamily(fontType)
    font.setPointSize(fontSize)
    font.setBold(False)
    font.setItalic(False)
    font.setWeight(50)
    font.setKerning(True)
    return font