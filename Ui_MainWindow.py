import pyperclip
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QMessageBox, QPushButton
from PyQt5.QtGui import QPixmap

from data import data
from utils import find_index
from styles import checkBox_styles, slider_styles, header_styles, button_styles, rounded_button_styles, password_label_styles, small_button_styles, small_label_styles
from font import custom_font
from dialog import show_dialog

passwords = data


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        self.name = ''
        self.password = ''
# ! =================================================================================
# ! ===========================   SHOW SCROLL AREA   ================================
# ! =================================================================================
        def copy_password(self,password):
            pyperclip.copy(password)
        def show_scroll_area(self=self):
            counter = 0
            self.scrollArea = QtWidgets.QScrollArea(self.list)
            self.scrollArea.setGeometry(QtCore.QRect(10, 79, 691, 341))
            self.scrollArea.setWidgetResizable(True)
            self.scrollArea.setVerticalScrollBarPolicy(
                QtCore.Qt.ScrollBarAlwaysOff)
            self.scrollArea.setHorizontalScrollBarPolicy(
                QtCore.Qt.ScrollBarAlwaysOff)
            self.scrollArea.setObjectName("scrollArea")
            self.scrollArea.setStyleSheet("border: none")
            self.scrollAreaWidgetContents = QtWidgets.QWidget()
            self.scrollAreaWidgetContents.setGeometry(
                QtCore.QRect(0, 0, 689, 339))
            self.scrollAreaWidgetContents.setObjectName(
                "scrollAreaWidgetContents")
            self.gridLayout = QtWidgets.QGridLayout(
                self.scrollAreaWidgetContents)
            self.gridLayout.setObjectName("gridLayout")
            for dic in passwords:
                self.copy_pushButton_list = QtWidgets.QPushButton(
                    self.scrollAreaWidgetContents)
                self.copy_pushButton_list.setMinimumSize(QtCore.QSize(30, 30))
                self.copy_pushButton_list.setMaximumSize(QtCore.QSize(30, 30))
                self.copy_pushButton_list.setStyleSheet(small_button_styles)
                self.copy_pushButton_list.setText("")
                self.copy_pushButton_list.setIcon(icon1)
                self.copy_pushButton_list.setIconSize(QtCore.QSize(20, 20))
                self.copy_pushButton_list.setObjectName(
                    f'copy_pushButton_list_{dic["name"]}')
                self.copy_pushButton_list.clicked.connect(
                    lambda self, password=dic["password"]: copy_password(self, password))
                self.gridLayout.addWidget(
                    self.copy_pushButton_list, 0 + counter, 2, 1, 1)
                self.delete_pushButton_list = QtWidgets.QPushButton(
                    self.scrollAreaWidgetContents)
                self.delete_pushButton_list.setMinimumSize(
                    QtCore.QSize(30, 30))
                self.delete_pushButton_list.setMaximumSize(
                    QtCore.QSize(30, 30))
                self.delete_pushButton_list.setStyleSheet(small_button_styles)
                self.delete_pushButton_list.setText("")
                icon3 = QtGui.QIcon()
                icon3.addPixmap(QtGui.QPixmap(
                    "./assets/delete-cross.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
                self.delete_pushButton_list.setIcon(icon3)
                self.delete_pushButton_list.setObjectName(
                    f'delete_pushButton_list_{dic["name"]}')
                self.delete_pushButton_list.clicked.connect(
                    lambda self, name=dic["name"]: delete_password(self, name))
                self.gridLayout.addWidget(
                    self.delete_pushButton_list, 0 + counter, 3, 1, 1)
                self.password_label_list = QtWidgets.QLabel(
                    self.scrollAreaWidgetContents)
                self.password_label_list.setMinimumSize(QtCore.QSize(280, 30))
                self.password_label_list.setMaximumSize(QtCore.QSize(280, 30))
                self.password_label_list.setStyleSheet(small_label_styles)
                self.password_label_list.setAlignment(QtCore.Qt.AlignCenter)
                self.password_label_list.setText(dic["password"])
                self.password_label_list.setObjectName(
                    f'password_label_list_{dic["name"]}')
                self.gridLayout.addWidget(
                    self.password_label_list, 0 + counter, 1, 1, 1)
                self.name_label_list = QtWidgets.QLabel(
                    self.scrollAreaWidgetContents)
                self.name_label_list.setMinimumSize(QtCore.QSize(280, 30))
                self.name_label_list.setMaximumSize(QtCore.QSize(280, 30))
                self.name_label_list.setStyleSheet(small_label_styles)
                self.name_label_list.setAlignment(QtCore.Qt.AlignCenter)
                self.name_label_list.setText(dic["name"])
                self.name_label_list.setObjectName(
                    f'name_label_list_{dic["name"]}')
                self.gridLayout.addWidget(
                    self.name_label_list, 0 + counter, 0, 1, 1)
                counter += 100
            spacerItem2 = QtWidgets.QSpacerItem(
                20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
            self.gridLayout.addItem(spacerItem2, 0 + counter, 1, 1, 1)
            self.scrollArea.setWidget(self.scrollAreaWidgetContents)
            self.scrollArea.show()
# ! =================================================================================
# ! ===========================   SAVE NEW PASSWORD   ===============================
# ! =================================================================================
        def save_new_password(self):
            if self.name == '':
                return show_dialog('Name must be provided', 'information', MainWindow)
            if self.password == '':
                return show_dialog('Password must be provided', 'information', MainWindow)
            index = find_index(passwords, 'name', self.name)
            if index != -1:
                return show_dialog(f'Name {self.name} already in use', 'information', MainWindow)
            passwords.append({
                "name": self.name,
                "password": self.password
            })
            f = open("data.py", "a")
            f.truncate(0)
            f.write(f'data={passwords}')
            f.close()
            self.create_password_lineEdit.clear()
            self.create_name_lineEdit.clear()
            show_scroll_area()
# ! =================================================================================
# ! ===========================   DELETE PASSWORD   =================================
# ! =================================================================================
        def delete_password(self, name):
            returnValue = show_dialog(
                f'Do you want to delete {name}?', 'warning', MainWindow)
            if returnValue == 1:
                return
            print(returnValue)
            index = find_index(passwords, 'name', name)
            del passwords[index]
            f = open("data.py", "a")
            f.truncate(0)
            f.write(f'data={passwords}')
            f.close()
            show_scroll_area()
# ! =================================================================================
# ! ===============================  SET NEW NAME  ==================================
# ! =================================================================================

        def set_new_name(self, text):
            self.name = text
# ! =================================================================================
# ! ==============================  SET NEW PASSWORD  ===============================
# ! =================================================================================

        def set_new_password(self, text):
            self.password = text

        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(711, 480)
        MainWindow.setMaximumSize(QtCore.QSize(711, 480))
        MainWindow.setMinimumSize(QtCore.QSize(711, 480))
        MainWindow.setStyleSheet('color:white;')
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.stackedWidget = QtWidgets.QStackedWidget(self.centralwidget)
        self.stackedWidget.setGeometry(QtCore.QRect(0, 0, 711, 480))
        self.stackedWidget.setMinimumSize(QtCore.QSize(711, 480))
        self.stackedWidget.setMaximumSize(QtCore.QSize(711, 500))
        self.stackedWidget.setStyleSheet("background-color: rgb(46, 52, 54);")
        self.stackedWidget.setObjectName("stackedWidget")
        self.generator = QtWidgets.QWidget()
        self.generator.setObjectName("generator")
        self.copy_label = QtWidgets.QLabel(self.generator)
        self.copy_label.setGeometry(QtCore.QRect(620, 220, 58, 51))
        self.copy_label.setStyleSheet("background-color: transparent;")
        self.copy_label.setText("")
        # self.copy_label.setPixmap(QtGui.QPixmap("./assets/copy.png"))
        self.copy_label.setAlignment(QtCore.Qt.AlignCenter)
        self.copy_label.setObjectName("copy_label")
        self.save_label = QtWidgets.QLabel(self.generator)
        self.save_label.setGeometry(QtCore.QRect(30, 430, 41, 41))
        self.save_label.setStyleSheet("background-color:transparent;")
        self.save_label.setText("")
        self.save_label.setPixmap(QtGui.QPixmap("../save-button .png"))
        self.save_label.setObjectName("save_label")
        self.password_label = QtWidgets.QLabel(self.generator)
        self.password_label.setGeometry(QtCore.QRect(20, 180, 671, 101))
        self.password_label.setFont(custom_font("Arial",20))
        self.password_label.setStyleSheet(password_label_styles)
        self.password_label.setAlignment(QtCore.Qt.AlignCenter)
        self.password_label.setObjectName("password_label")

        self.label_copied = QtWidgets.QLabel(self.generator)
        self.label_copied.setGeometry(QtCore.QRect(10, 280, 691, 60))
        self.label_copied.setText("")
        self.label_copied.setStyleSheet(
            "color:rgb(194, 108, 219);font-size:20px;")
        self.label_copied.setAlignment(QtCore.Qt.AlignCenter)
        self.label_copied.setObjectName("label_copied")

        self.title_label = QtWidgets.QLabel(self.generator)
        self.title_label.setGeometry(QtCore.QRect(10, 10, 691, 61))

        self.title_label.setFont(custom_font("Kalapi",24))
        self.title_label.setStyleSheet(header_styles)
        self.title_label.setAlignment(QtCore.Qt.AlignCenter)
        self.title_label.setObjectName("title_label")
        self.generate_pushButton = QtWidgets.QPushButton(self.generator)
        self.generate_pushButton.setGeometry(QtCore.QRect(520, 430, 171, 31))
        self.generate_pushButton.setStyleSheet(button_styles)
        self.generate_pushButton.setObjectName("generate_pushButton")
        self.horizontalLayoutWidget = QtWidgets.QWidget(self.generator)
        self.horizontalLayoutWidget.setGeometry(QtCore.QRect(20, 100, 671, 51))
        self.horizontalLayoutWidget.setObjectName("horizontalLayoutWidget")
        self.horizontalLayout = QtWidgets.QHBoxLayout(
            self.horizontalLayoutWidget)
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.length_label = QtWidgets.QLabel(self.horizontalLayoutWidget)
        self.length_label.setMinimumSize(QtCore.QSize(100, 0))
        self.length_label.setMaximumSize(QtCore.QSize(100, 16777215))
        self.length_label.setFont(custom_font("Kalapi",12))
        self.length_label.setStyleSheet("color: white")
        self.length_label.setAlignment(
            QtCore.Qt.AlignLeading | QtCore.Qt.AlignLeft | QtCore.Qt.AlignVCenter)
        self.length_label.setObjectName("length_label")
        self.horizontalLayout.addWidget(self.length_label)
        spacerItem = QtWidgets.QSpacerItem(
            40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)
        self.slider = QtWidgets.QSlider(self.horizontalLayoutWidget)
        self.slider.setMinimumSize(QtCore.QSize(400, 0))
        self.slider.setMaximumSize(QtCore.QSize(400, 16777215))
        self.slider.setMinimum(1)
        self.slider.setMaximum(32)
        self.slider.setSliderPosition(10)
        self.slider.setOrientation(QtCore.Qt.Horizontal)
        self.slider.setStyleSheet(slider_styles)
        self.slider.setInvertedAppearance(False)
        self.slider.setInvertedControls(False)
        self.slider.setObjectName("slider")
        self.horizontalLayout.addWidget(self.slider)
        spacerItem1 = QtWidgets.QSpacerItem(
            40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem1)
        self.length_number_label = QtWidgets.QLabel(
            self.horizontalLayoutWidget)
        self.length_number_label.setMinimumSize(QtCore.QSize(30, 0))
        self.length_number_label.setMaximumSize(QtCore.QSize(30, 16777215))
        self.length_number_label.setFont(custom_font("Arial",20))
        self.length_number_label.setStyleSheet("color:rgb(194, 108, 219);")
        self.length_number_label.setAlignment(QtCore.Qt.AlignCenter)
        self.length_number_label.setObjectName("length_number_label")
        self.horizontalLayout.addWidget(self.length_number_label)
        self.list_pushButton = QtWidgets.QPushButton(self.generator)
        self.list_pushButton.setGeometry(QtCore.QRect(20, 420, 50, 50))
        self.list_pushButton.setMaximumSize(QtCore.QSize(50, 50))
        self.list_pushButton.setStyleSheet(rounded_button_styles)
        self.list_pushButton.setText("")
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap("./assets/list.png"),
                       QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.list_pushButton.setIcon(icon)
        self.list_pushButton.setIconSize(QtCore.QSize(24, 24))
        self.list_pushButton.setObjectName("list_pushButton")
        self.copy_pushButton = QtWidgets.QPushButton(self.generator)
        self.copy_pushButton.setGeometry(QtCore.QRect(620, 210, 58, 41))
        self.copy_pushButton.setMinimumSize(QtCore.QSize(58, 41))
        self.copy_pushButton.setStyleSheet("background-color: transparent;\n"
                                           "color:white;\n"
                                           "border-radius: 6px;\n"
                                           "")
        self.copy_pushButton.setText("")
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap("./assets/copy.png"),
                        QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.copy_pushButton.setIcon(icon1)
        self.copy_pushButton.setIconSize(QtCore.QSize(24, 24))
        self.copy_pushButton.setObjectName("copy_pushButton")
        self.uppercase_checkBox = QtWidgets.QCheckBox(self.generator)
        self.uppercase_checkBox.setGeometry(QtCore.QRect(10, 340, 160, 20))
        self.uppercase_checkBox.setLayoutDirection(QtCore.Qt.RightToLeft)
        self.uppercase_checkBox.setTristate(False)
        self.uppercase_checkBox.setStyleSheet(checkBox_styles)
        self.uppercase_checkBox.setObjectName("uppercase_checkBox")
        self.lowercase_checkBox = QtWidgets.QCheckBox(self.generator)
        self.lowercase_checkBox.setGeometry(QtCore.QRect(190, 340, 160, 20))
        self.lowercase_checkBox.setLayoutDirection(QtCore.Qt.RightToLeft)
        self.lowercase_checkBox.setStyleSheet(checkBox_styles)
        self.lowercase_checkBox.setObjectName("lowercase_checkBox")
        self.symbol_checkBox = QtWidgets.QCheckBox(self.generator)
        self.symbol_checkBox.setGeometry(QtCore.QRect(360, 340, 160, 20))
        self.symbol_checkBox.setLayoutDirection(QtCore.Qt.RightToLeft)
        self.symbol_checkBox.setStyleSheet(checkBox_styles)
        self.symbol_checkBox.setObjectName("symbol_checkBox")
        self.number_checkBox = QtWidgets.QCheckBox(self.generator)
        self.number_checkBox.setGeometry(QtCore.QRect(520, 340, 160, 20))
        self.number_checkBox.setLayoutDirection(QtCore.Qt.RightToLeft)
        self.number_checkBox.setStyleSheet(checkBox_styles)
        self.number_checkBox.setObjectName("number_checkBox")
        self.stackedWidget.addWidget(self.generator)
        self.list = QtWidgets.QWidget()
        self.list.setObjectName("list")
        self.title_label_2 = QtWidgets.QLabel(self.list)
        self.title_label_2.setGeometry(QtCore.QRect(10, 10, 691, 61))
        self.title_label_2.setFont(custom_font("Kalapi",24))
        self.title_label_2.setStyleSheet(header_styles)
        self.title_label_2.setAlignment(QtCore.Qt.AlignCenter)
        self.title_label_2.setObjectName("title_label_2")
        self.back_pushButton = QtWidgets.QPushButton(self.list)
        self.back_pushButton.setGeometry(QtCore.QRect(10, 20, 61, 41))
        self.back_pushButton.setMinimumSize(QtCore.QSize(61, 41))
        self.back_pushButton.setStyleSheet("background-color: transparent;\n"
                                           "color:white;\n"
                                           "border-radius: 6px;\n"
                                           )
        self.back_pushButton.setText("")
        icon2 = QtGui.QIcon()
        icon2.addPixmap(QtGui.QPixmap(
            "./assets/curve-down-left-arrow.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.back_pushButton.setIcon(icon2)
        self.back_pushButton.setIconSize(QtCore.QSize(32, 32))
        self.back_pushButton.setObjectName("back_pushButton")
        # ! ==========================================================================
        self.horizontalLayoutWidget_2 = QtWidgets.QWidget(self.list)
        self.horizontalLayoutWidget_2.setGeometry(
            QtCore.QRect(10, 430, 691, 41))
        self.horizontalLayoutWidget_2.setObjectName("horizontalLayoutWidget_2")
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout(
            self.horizontalLayoutWidget_2)
        self.horizontalLayout_2.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.create_name_lineEdit = QtWidgets.QLineEdit(
            self.horizontalLayoutWidget_2)
        self.create_name_lineEdit.setMinimumSize(QtCore.QSize(280, 30))
        self.create_name_lineEdit.setMaximumSize(QtCore.QSize(280, 30))
        self.create_name_lineEdit.setStyleSheet("border-radius: 6px;\n"
                                                "background-color:rgba(255,255,255,0.2);\n"
                                                "color: white;\n"
                                                "border: 1px solid rgba(194, 108, 219);")
        self.create_name_lineEdit.setAlignment(QtCore.Qt.AlignCenter)
        self.create_name_lineEdit.setClearButtonEnabled(False)
        self.create_name_lineEdit.textChanged.connect(
            lambda text: set_new_name(self, text))
        self.create_name_lineEdit.setObjectName("create_name_lineEdit")
        self.horizontalLayout_2.addWidget(self.create_name_lineEdit)
        self.create_password_lineEdit = QtWidgets.QLineEdit(
            self.horizontalLayoutWidget_2)
        self.create_password_lineEdit.setMinimumSize(QtCore.QSize(280, 30))
        self.create_password_lineEdit.setMaximumSize(QtCore.QSize(280, 30))
        self.create_password_lineEdit.setStyleSheet("border-radius: 6px;\n"
                                                    "background-color:rgba(255,255,255,0.2);\n"
                                                    "color: white;\n"
                                                    "border: 1px solid rgba(194, 108, 219);")
        self.create_password_lineEdit.setAlignment(QtCore.Qt.AlignCenter)
        self.create_password_lineEdit.setClearButtonEnabled(False)
        self.create_password_lineEdit.textChanged.connect(
            lambda text: set_new_password(self, text))
        self.create_password_lineEdit.setObjectName("create_password_lineEdit")
        self.horizontalLayout_2.addWidget(self.create_password_lineEdit)
        self.save_pushButton = QtWidgets.QPushButton(
            self.horizontalLayoutWidget_2)
        self.save_pushButton.setMinimumSize(QtCore.QSize(75, 30))
        self.save_pushButton.setMaximumSize(QtCore.QSize(75, 30))
        self.save_pushButton.setStyleSheet(button_styles)
        self.save_pushButton.clicked.connect(lambda: save_new_password(self))
        self.save_pushButton.setObjectName("./assets/save_pushButton")
        self.horizontalLayout_2.addWidget(self.save_pushButton)
        self.stackedWidget.addWidget(self.list)
        MainWindow.setCentralWidget(self.centralwidget)
        show_scroll_area(self)
        self.retranslateUi(MainWindow)
        self.stackedWidget.setCurrentIndex(1)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate(
            "MainWindow", "Passwords Manager"))
        self.password_label.setText(_translate("MainWindow", ""))
        self.title_label.setText(_translate(
            "MainWindow", "PASSWORD  GENERATOR"))
        self.generate_pushButton.setText(_translate("MainWindow", "GENERATE"))
        self.length_label.setText(_translate("MainWindow", "LENGTH"))
        self.length_number_label.setText(_translate("MainWindow", "24"))
        self.uppercase_checkBox.setText(
            _translate("MainWindow", "UPPERCASE  ( A-Z )"))
        self.lowercase_checkBox.setText(
            _translate("MainWindow", "LOWERCASE  ( a-z )"))
        self.symbol_checkBox.setText(
            _translate("MainWindow", "SYMBOL  ( # * @ )"))
        self.number_checkBox.setText(_translate("MainWindow", "NUMBER  (0-9)"))
        self.title_label_2.setText(_translate("MainWindow", "SAVED PASSWORDS"))
        self.create_name_lineEdit.setPlaceholderText(
            _translate("MainWindow", "Type name here"))
        self.create_password_lineEdit.setPlaceholderText(
            _translate("MainWindow", "Type or paste password here"))
        self.save_pushButton.setText(_translate("MainWindow", "SAVE"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())
