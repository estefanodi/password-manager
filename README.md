# Password Manager App

Simple password manager desktop application for MacOS and Ubuntu.

<br>

### Built with 

* Python3

* PyQt5

* Qt Creator

<br>
<br>
<img src="https://res.cloudinary.com/dzixa3xgx/image/upload/v1619705853/u7ubky9brnhojxb2zdzj.png" alt="splash screen" width='500px'>
<br>
<br>
<br>
<br>
<img src="https://res.cloudinary.com/dzixa3xgx/image/upload/v1619710064/pfuy09om7lprynqmffeo.png" alt="splash screen" width='500px'>
<br>
<br>
<br>
<br>
<img src="https://res.cloudinary.com/dzixa3xgx/image/upload/v1619710064/m5gyttkos6cmau6au9my.png" alt="splash screen" width='500px'>
